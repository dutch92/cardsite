const gulp = require('gulp');
const stylus = require('gulp-stylus');
const sourcemaps = require('gulp-sourcemaps');
const gulpIf = require('gulp-if');
const del = require('del');
const debug = require('gulp-debug');
const newer = require('gulp-newer');
const cached = require('gulp-cached');
const path = require('path');
const browserSync = require('browser-sync').create();

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

gulp.task('styles', function() {
  return gulp.src('frontend/styles/main.styl')
    .pipe(cached('styles'))
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(stylus())
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(debug({ title: 'Styles' }))
    .pipe(gulp.dest('public'))
});

gulp.task('clean', function() {
  return del('public');
});

gulp.task('assets', function() {
  return gulp.src('frontend/assets/**')
    .pipe(newer('public'))
    .pipe(debug({ title: 'Assets' }))
    .pipe(gulp.dest('public'));
});

gulp.task('watch', function() {
  gulp.watch('frontend/styles/**/*.*', gulp.series('styles')).on('unlink', function(filepath) {
    delete cached.caches.styles[path.resolve(filepath)];
  });
  gulp.watch('frontend/assets/**/*.*', gulp.series('assets'));
});

gulp.task('build', gulp.series('clean', gulp.parallel('styles', 'assets')));

gulp.task('serve', function() {
  browserSync.init({
    server: 'public',
  });

  browserSync.watch('public/**/*.*').on('change', browserSync.reload);
});

gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'serve')));